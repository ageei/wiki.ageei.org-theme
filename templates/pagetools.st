<div class="pageTools">
  <fieldset>
    <legend>Cette page</legend>
    <ul>
      <li><a href="$base$/_showraw$pageUrl$$if(revision)$?revision=$revision$$endif$">Source de la page</a></li>
      <li><a href="$base$$pageUrl$?printable$if(revision)$&amp;revision=$revision$$endif$">Version imprimable</a></li>
      $if(feed)$
      <li><a href="$base$/_feed$pageUrl$" type="application/atom+xml" rel="alternate" title="Flux Atom de cette page">Flux Atom</a> <img alt="feed icon" src="$base$/img/icons/feed.png"/></li>
      $endif$
    </ul>
    $exportbox$
  </fieldset>
</div>

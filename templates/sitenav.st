<div class="sitenav">
  <fieldset>
    <legend>Site</legend>
    <ul>
      <li><a href="$base$/">Accueil</a></li>
      <li><a href="$base$/_index">Toutes les pages</a></li>
      <li><a href="$base$/_categories">Cat&eacute;gories</a></li>
      <li><a href="$base$/_random">Page au hasard</a></li>
      <li><a href="$base$/_activity">Activit&eacute; r&eacute;cente</a></li>
      $if(feed)$
      <li><a href="$base$/_feed/" type="application/atom+xml" rel="alternate" title="Flux Atom">Flux Atom</a> <img alt="feed icon" src="$base$/img/icons/feed.png"/></li>
      $endif$
      <li><a href="$base$/Help">Aide</a></li>
    </ul>
    <form action="$base$/_search" method="get" id="searchform">
     <input type="text" name="patterns" id="patterns"/>
     <input type="submit" name="search" id="search" value="Chercher"/>
    </form>
    <form action="$base$/_go" method="post" id="goform">
      <input type="text" name="gotopage" id="gotopage"/>
      <input type="submit" name="go" id="go" value="Aller"/>
    </form>
  </fieldset>
</div>

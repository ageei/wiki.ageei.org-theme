# wiki.ageei.org-theme

Ce dépôt contient le thème/style du [wiki de l'AGEEI][1]. Il permet
à ses membres de partager des ressources ou de l'information qui peuvent
leur être utile dans leur parcours académique, professionel et personel.


## Comment contribuer

Le style du wiki est géré indépendamment de son contenu, mais nous
utilisons git pour le gérer lui aussi. Les modifications au thème se
font par le biais de "demandes de changements" via le
[dépôt git officiel][2]. Si vous n'êtes pas familier avec git et
souhaitez quand même faire une contribution, nous vous invitons à
créer un [ticket][3] décrivant le changement que vous souhaitez
effectuer.

### Exemple

Créer une fourche du projet.

**TODO insérer capture d'écran**

```
git clone https://gitlab.com/mon-utilisateur/wiki.ageei.org-theme
cd wiki.ageei.org-theme
editor templates/footer.st # faites vos changements
git add templates/footer.st
git commit # décrivez le changement et pourquoi
git push
```

Faites votre MR.

**TODO insérer capture d'écran**

[1]: https://wiki.ageei.org/
[2]: https://gitlab.com/ageei/wiki.ageei.org-theme/
[3]: https://gitlab.com/ageei/wiki.ageei.org-theme/issues
